package stepDefs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import constants.Constants;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hooks
{
	
	public static WebDriver driver;
	
	@Before
	public void beforeHooks(Scenario scenario) throws InterruptedException 
	{
		
		System.setProperty("webdriver.chrome.driver","C:/Users/Snehal/Downloads/chromedriver.exe");
		driver = new ChromeDriver();
		//Constants.setDriver(driver);
		
		driver.get("https://www.google.com");
		driver.manage().window().maximize();		
		Thread.sleep(5000);
	}	
		
	@After
	public void afterHooks(Scenario scenario)
	{
		driver.quit();
	}
}