package stepDefs;

import cucumber.api.java.en.When;
import pages.GmailPage;

import org.openqa.selenium.WebDriver;
//import cucumber.api.java.en.Then;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;

public class LoginStepDefinitions
{
	
	GmailPage gmailpage;
	
	public LoginStepDefinitions() {
		gmailpage = new GmailPage();
	}
		
//	LoginStepDefinitions()
//	{
//		 
//	}
//	
	
	Hooks hooks;
	
	    @Given("^user navigates to google accounts page$")
	    public void user_navigates_to_google_accounts_page() throws Throwable 
	    {
		/*
		 * System.out.println("Hi"); try { System.out.println("Hiii");
		 * hooks.beforeHooks();
		 * 
		 * System.out.println("bye");
		 * 
		 * }catch (InterruptedException e) { // TODO Auto-generated catch block
		 * 
		 * System.out.println(e); }
		 */
	    	
	     	gmailpage.open_gmail();	    	      
	    	
	    }

	    @When("^user enters the email$")
	    public void user_enters_the_email(String mail) throws Throwable 
	    {
	    	gmailpage.enter_email(mail);
	    }

	    @And("^user enters the password$")
	    public void user_enters_the_password(String pass) throws Throwable 
	    {
	    	gmailpage.enter_password(pass);
	    	//hooks.afterHooks();
	    }
	    
	  /*  @Then("^user is navigated to Gmail Account$")
	    public void user_is_navigated_to_gmail_account() throws Throwable
	    {
	        
	    }*/
}
	