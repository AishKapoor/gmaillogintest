package runner;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
		monochrome = true,
		features = "Feature/",
		format = {"pretty"},
		glue = {"stepDefs","constants","pages"},
		
		tags = {"@GmailLogin"}
	)

public class Runner
{	
	
	
}